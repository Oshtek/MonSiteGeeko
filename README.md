## Geeko

    Projet quasiment terminé : Etant donné que l'assemblée générale est récente, la rubrique "Membres du conseil d'administration est indisponible"
    Media Query fonctionnel.
    En attente du retour des professeur pour un éventuel changement de design.
    29/10/2017 : -Pour des raisons esthétiques, remplacement des textes par des PDF
                 -Ajout d'un flavicon à côté du titre
    30/10/2017 : -Ajout d'un bouton retour à l'accueil dans chaque page
                 -Correction du fichier style.css 
    31/10/2017 : Après relecture, ajout balise fermante manquante.